<?php
use yii\widgets\DetailView;
use yii\bootstrap4\Html;

echo DetailView::widget([
    "model" => $model
]);

echo Html::a("Estas seguro que deseas eliminar la nota",
        ['site/eliminar1','id' => $model->id]);