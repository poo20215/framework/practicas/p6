<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Anadir */
/* @var $form ActiveForm */
?>
<div class="anadir">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'titulo') ?>
        <?= $form->field($model, 'contenido')->textarea() ?>
    
        <div class="form-group">
            <?= Html::submitButton('Añadir Nota', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- anadir -->
