<?php
use yii\helpers\Html;
?>

<br><br>

<div>
    <h4><?= $model->titulo ?></h4>
    <p><?= $model->contenido ?></p>
    <?= Html::a("Eliminar Nota",["site/eliminar", 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
</div>

<hr/><br>



