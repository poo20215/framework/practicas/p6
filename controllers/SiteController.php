<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Notas;
use yii\data\ActiveDataProvider;
use app\models\Anadir;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionNotas()
    {
        $dataProvider = new ActiveDataProvider([
        'query' => Notas::find()]);
        return $this->render("notas",["dataProvider" => $dataProvider]);
    }
    
    public function actionAnadir()
    {
        $model = new Anadir();

        if ($model->load(Yii::$app->request->post())) 
        {
            if ($model->validate()) 
            {
                $datos= new Notas;
                $datos->titulo=$model->titulo;
                $datos->contenido=$model->contenido;
                $datos->save();
                return $this->redirect("notas");  //por defectos renderiza a acciones del controlador site 
            }
        }

        return $this->render('anadir', [
            'model' => $model,
        ]);
        
    }
    
    public function actionEliminar($id)
    {
        $model = Notas::findOne($id);
        
        //llamamos a una ventana para verificar el borrado
        return $this->render("verificacion",["model"=>$model]);
    }
    
    public function actionEliminar1($id)
    {
        $model= Notas::findOne($id);
        $model->delete();
        return $this->redirect("notas");
    }
    
}
